install:
	pnpm install

start:
	node_modules/.bin/avris-daemonise start assets node_modules/.bin/encore dev-server
	sleep 10
	node_modules/.bin/avris-daemonise start webserver bin/www

stop:
	node_modules/.bin/avris-daemonise stop assets
	node_modules/.bin/avris-daemonise stop webserver

deploy: install
	pnpm run build
