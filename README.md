# Avris Generator

Generate and validate numbers and identifiers like IBAN, ISBN, EAN, PESEL, BSN, UTR, NINO, NIF, DNI, passwords...

## GUI & API

    yarn
    yarn build
    yarn start
    
## Library

    yarn add avris-generator
    const gm = require('avris-generator');
    
    const pesel = gm.generate('PL', 'pesel', {gender: 'm'});
    
    try {
        const data = gm.validate('PL', 'pesel', '12345678900');
        // valid
    } catch (e) {
        // invalid
        alert(e.message);
    }
    
## Copyright

* **Author:** Andrea Vos [(avris.it)](https://avris.it)
* **Licence:** [OQL](https://oql.avris.it/license?c=Andrea%20Vos%7Chttps://avris.it)
