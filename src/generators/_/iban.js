const Generator = require('../../abstractGenerator');
const {randomElement, randomise, letterToNumber, bigModulo} = require('../../helpers');
const {digits, upper} = require('../../sets');
const handleParams = require('../../handleParams');

module.exports = class IBAN extends Generator {
    static get countries() {
        return {
            'AD': {
                len: 24,
                format: ['8n', '12c'],
                example: 'AD12 0001 2030 2003 5910 0100',
            },
            'AE': {
                len: 23,
                format: ['3n', '16n'],
                example: 'AE07 0331 2345 6789 0123 456',
            },
            'AL': {
                len: 28,
                format: ['8n', '16c'],
                example: 'AL47 2121 1009 0000 0002 3569 8741',
                nationalCheck: true,
            },
            'AT': {
                len: 20,
                format: ['16n'],
                example: 'AT61 1904 3002 3457 3201',
            },
            'AZ': {
                len: 28,
                format: ['4c', '20n'],
                example: 'AZ21 NABZ 0000 0000 1370 1000 1944',
            },
            'BA': {
                len: 20,
                format: ['16n'],
                example: 'BA39 1290 0794 0102 8494',
                nationalCheck: true,
                ibanCheck: '39',
            },
            'BE': {
                len: 16,
                format: ['12n'],
                example: 'BE68 5390 0754 7034',
                nationalCheck: true,
            },
            'BG': {
                len: 22,
                format: ['4a', '6n', '8c'],
                example: 'BG80 BNBG 9661 1020 3456 78',
            },
            'BH': {
                len: 22,
                format: ['4a', '14c'],
                example: 'BH67 BMAG 0000 1299 1234 56',
            },
            'BR': {
                len: 29,
                format: ['23n', '1a' ,'1c'],
                example: 'BR18 0036 0305 0000 1000 9795 493C 1',
            },
            'CH': {
                len: 21,
                format: ['5n', '12c'],
                example: 'CH93 0076 2011 6238 5295 7',
            },
            'CR': {
                len: 21,
                format: [0, '17n'],
                example: 'CR05 0152 0200 1026 2840 66',
            },
            'CY': {
                len: 28,
                format: ['8n', '16c'],
                example: 'CY17 0020 0128 0000 0012 0052 7600',
            },
            'CZ': {
                len: 24,
                format: ['20n'],
                example: 'CZ65 0800 0000 1920 0014 5399',
            },
            'DE': {
                len: 22,
                format: ['18n'],
                example: 'DE89 3704 0044 0532 0130 00',
            },
            'DK': {
                len: 18,
                format: ['14n'],
                example: 'DK50 0040 0440 1162 43',
                nationalCheck: true,
            },
            'DO': {
                len: 28,
                format: ['4a', '20n'],
                example: 'DO28 BAGR 0000 0001 2124 5361 1324',
            },
            'EE': {
                len: 20,
                format: ['16n'],
                example: 'EE38 2200 2210 2014 5685',
                nationalCheck: true,
            },
            'EG': {
                len: 29,
                format: ['25n'],
                //example: '',
            },
            'ES': {
                len: 24,
                format: ['20n'],
                example: 'ES91 2100 0418 4502 0005 1332',
                nationalCheck: true,
            },
            'FI': {
                len: 18,
                format: ['14n'],
                example: 'FI21 1234 5600 0007 85',
                nationalCheck: true,
            },
            'FR': {
                len: 27,
                format: ['10n', '11c', '2n'],
                example: 'FR14 2004 1010 0505 0001 3M02 606',
                nationalCheck: true,
            },
            'FO': {
                len: 18,
                format: ['14n'],
                example: 'FO62 6460 0001 6316 34',
                nationalCheck: true,
            },
            'GB': {
                len: 22,
                format: ['4a', '14n'],
                example: 'GB29 NWBK 6016 1331 9268 19',
            },
            'GE': {
                len: 22,
                format: ['2c', '16n'],
                example: 'GE29 NB00 0000 0101 9049 17',
            },
            'GI': {
                len: 23,
                format: ['4a', '15c'],
                example: 'GI75 NWBK 0000 0000 7099 453',
            },
            'GL': {
                len: 18,
                format: ['14n'],
                example: 'GL89 6471 0001 0002 06',
                nationalCheck: true,
            },
            'GR': {
                len: 27,
                format: ['7n', '16c'],
                example: 'GR16 0110 1250 0000 0001 2300 695',
            },
            'GT': {
                len: 28,
                format: ['4c', '20c'],
                example: 'GT82 TRAJ 0102 0000 0012 1002 9690',
            },
            'HR': {
                len: 21,
                format: ['17n'],
                example: 'HR12 1001 0051 8630 0016 0',
            },
            'HU': {
                len: 28,
                format: ['24n'],
                example: 'HU42 1177 3016 1111 1018 0000 0000',
                nationalCheck: true,
            },
            'IE': {
                len: 22,
                format: ['4c', '14n'],
                example: 'IE29 AIBK 9311 5212 3456 78',
            },
            'IL': {
                len: 23,
                format: ['19n'],
                example: 'IL62 0108 0000 0009 9999 999',
            },
            'IQ': {
                len: 23,
                format: ['4a', '15n'],
                example: 'IQ98 NBIQ 8501 2345 6789 012',
            },
            'IS': {
                len: 26,
                format: ['22n'],
                example: 'IS14 0159 2600 7654 5510 7303 39',
            },
            'IT': {
                len: 27,
                format: ['1a', '10n', '12c'],
                example: 'IT60 X054 2811 1010 0000 0123 456',
                nationalCheck: true,
            },
            'JO': {
                len: 30,
                format: ['4a', '22n'],
                example: 'JO94 CBJO 0010 0000 0000 0131 000302',
            },
            'KW': {
                len: 30,
                format: ['4a', '22c'],
                example: 'KW81 CBKU 0000 0000 0000 1234 560101',
            },
            'KZ': {
                len: 20,
                format: ['3n', '13c'],
                example: 'KZ86 125K ZT50 0410 0100',
            },
            'LB': {
                len: 28,
                format: ['4n', '20c'],
                example: 'LB62 0999 0000 0001 0019 0122 9114',
            },
            'LC': {
                len: 32,
                format: ['4a', '24c'],
                example: 'LC55 HEMM 0001 0001 0012 0012 00023015',
            },
            'LI': {
                len: 21,
                format: ['5n', '12c'],
                example: 'LI21 0881 0000 2324 013A A',
            },
            'LT': {
                len: 20,
                format: ['16n'],
                example: 'LT12 1000 0111 0100 1000',
            },
            'LU': {
                len: 20,
                format: ['3n', '13c'],
                example: 'LU28 0019 4006 4475 0000',
            },
            'LV': {
                len: 21,
                format: ['4a', '13c'],
                example: 'LV80 BANK 0000 4351 9500 1',
            },
            'LY': {
                len: 25,
                format: ['21n'],
                //example: '',
            },
            'MC': {
                len: 27,
                format: ['10n', '11c', '2n'],
                example: 'MC58 1122 2000 0101 2345 6789 030',
                nationalCheck: true,
            },
            'MD': {
                len: 24,
                format: ['2c', '18c'],
                example: 'MD24 AG00 0225 1000 1310 4168',
            },
            'ME': {
                len: 22,
                format: ['18n'],
                example: 'ME25 5050 0001 2345 6789 51',
                nationalCheck: true,
                ibanCheck: '25',
            },
            'MK': {
                len: 19,
                format: ['3n', '10c', '2n'],
                example: 'MK07 2501 2000 0058 984',
                nationalCheck: true,
                ibanCheck: '07',
            },
            'MR': {
                len: 27,
                format: ['23n'],
                example: 'MR13 0002 0001 0100 0012 3456 753',
                nationalCheck: true,
                ibanCheck: '13',
            },
            'MT': {
                len: 31,
                format: ['4a', '5n', '18c'],
                example: 'MT84 MALT 0110 0001 2345 MTLC AST0 01S',
            },
            'MU': {
                len: 30,
                format: ['4a', '16n', 0, 0, 0, '3a'],
                example: 'MU17 BOMM 0101 1010 3030 0200 000M UR',
            },
            'NL': {
                len: 18,
                format: ['4a', '10n'],
                example: 'NL91 ABNA 0417 1643 00',
            },
            'NO': {
                len: 15,
                format: ['11n'],
                example: 'NO93 8601 1117 947',
                nationalCheck: true,
            },
            'PK': {
                len: 24,
                format: ['4c', '16n'],
                example: 'PK36 SCBL 0000 0011 2345 6702',
            },
            'PL': {
                len: 28,
                format: ['24n'],
                example: 'PL61 1090 1014 0000 0712 1981 2874',
                nationalCheck: true,
            },
            'PS': {
                len: 29,
                format: ['4c', '21n'],
                example: 'PS92 PALS 0000 0000 0400 1234 5670 2',
            },
            'PT': {
                len: 25,
                format: ['21n'],
                example: 'PT50 0002 0123 1234 5678 9015 4',
                nationalCheck: true,
                ibanCheck: '50',
            },
            'QA': {
                len: 29,
                format: ['4a', '21c'],
                example: 'QA58 DOHB 0000 1234 5678 90AB CDEF G',
            },
            'RO': {
                len: 24,
                format: ['4a', '16c'],
                example: 'RO49 AAAA 1B31 0075 9384 0000',
            },
            'RS': {
                len: 22,
                format: ['18n'],
                example: 'RS35 2600 0560 1001 6113 79',
                nationalCheck: true,
                ibanCheck: '35',
            },
            'SA': {
                len: 24,
                format: ['2n', '18c'],
                example: 'SA03 8000 0000 6080 1016 7519',
            },
            'SC': {
                len: 31,
                format: ['4a', '20n', '3a'],
                example: 'SC18 SSCB 1101 0000 0000 0000 1497 USD',
            },
            'SD': {
                len: 18,
                format: ['14n'],
                // example: '',
            },
            'SE': {
                len: 24,
                format: ['20n'],
                example: 'SE45 5000 0000 0583 9825 7466',
            },
            'SI': {
                len: 19,
                format: ['15n'],
                example: 'SI56 2633 0001 2039 086',
                nationalCheck: true,
                ibanCheck: '56',
            },
            'SK': {
                len: 24,
                format: ['20n'],
                example: 'SK31 1200 0000 1987 4263 7541',
            },
            'ST': {
                len: 25,
                format: ['21n'],
                example: 'ST68 0002 0001 0192 1942 1011 2',
            },
            'SV': {
                len: 28,
                format: ['4a', '20n'],
                example: 'SV 62 CENR 00000000000000700025',
            },
            'SM': {
                len: 27,
                format: ['1a', '10n', '12c'],
                example: 'SM86 U032 2509 8000 0000 0270 100',
                nationalCheck: true,
            },
            'TL': {
                len: 23,
                format: ['19n'],
                example: 'TL38 0080 0123 4567 8910 157',
                nationalCheck: true,
                ibanCheck: '38',
            },
            'TN': {
                len: 24,
                format: ['20n'],
                example: 'TN59 1000 6035 1835 9847 8831',
                nationalCheck: true,
                ibanCheck: '59',
            },
            'TR': {
                len: 26,
                format: ['5n', 0, '16c'],
                example: 'TR33 0006 1005 1978 6457 8413 26',
            },
            'UA': {
                len: 29,
                format: ['6n', '19c'],
                example: 'UA21 3223 1300 0002 6007 2335 6600 1',
            },
            'VA': {
                len: 22,
                format: ['3n', '15n'],
                exmaple: 'VA59 001 1230 0001 2345 678',
            },
            'VG': {
                len: 24,
                format: ['4c', '16n'],
                example: 'VG96 VPVG 0000 0123 4567 8901',
            },
            'XK': {
                len: 20,
                format: ['4n', '10n', '2n'],
                example: 'XK05 1212 0123 4567 8906',
            }
        }
    }

    params() {
        return {
            country: {
                type: 'string',
                values: Object.keys(IBAN.countries),
            },
        };
    }

    generate(params = {}) {
        params = handleParams(this.params(), params);

        const countryCode = params.country || randomElement(Object.keys(IBAN.countries));

        let iban = '';
        if (IBAN.countries[countryCode].ibanCheck) {
            while (iban.substr(2, 2) !== IBAN.countries[countryCode].ibanCheck) {
                iban = this._doGenerate(countryCode);
            }
        } else {
            iban = this._doGenerate(countryCode);
        }

        return iban.replace(/\S{4}/g, m => m + ' ').trim();
    }

    _doGenerate(countryCode) {
        const nr = IBAN.countries[countryCode].format.map(el => {
            if (typeof el === 'number') {
                return '' + el;
            }

            const count = el.substr(0, el.length - 1);
            const type = el.substr(el.length - 1);

            return randomise(
                type === 'a' ? upper : digits,
                parseInt(count)
            );
        }).join('');

        const nrBig = this._lettersToValues(nr + countryCode + '00');
        const modulo = bigModulo(nrBig, 97);
        const checksum = ((97 - modulo) + 1) % 97;

        return countryCode + checksum.toString().padStart(2, '0') + nr;
    }

    validate(value) {
        value = value.replace(/\s/g, '');

        if (!value.match(/^[A-Z]{2}[0-9]{2}[A-Z0-9]+$/gi)) {
            throw new Error('format')
        }

        const countryCode = value.slice(0, 2).toUpperCase();

        if (!(countryCode in IBAN.countries)) {
            throw new Error('countryCode')
        }

        if (value.length !== IBAN.countries[countryCode].len) {
            throw new Error('length')
        }

        const regex = IBAN.countries[countryCode].format.map(el => {
            if (typeof el === 'number') {
                return '' + el;
            }

            const count = el.substr(0, el.length - 1);
            const type = el.substr(el.length - 1);

            return {
                n: '[0-9]',
                a: '[A-Z]',
                c: '[A-Z0-9]',
            }[type] + '{' + count + '}';
        }).join('');

        if (!value.match(new RegExp('^[A-Z]{2}[0-9]{2}' + regex + '$', 'gi'))) {
            throw new Error('format')
        }

        if (IBAN.countries[countryCode].ibanCheck) {
            if (IBAN.countries[countryCode].ibanCheck !== value.substr(2, 2)) {
                throw new Error('checksum')
            }
        } else {
            value = this._lettersToValues(value.substr(4) + value.substr(0, 4))

            if (bigModulo(value, 97) !== 1) {
                throw new Error('checksum')
            }
        }

        return {
            country: countryCode,
            nationalCheck: IBAN.countries[countryCode].nationalCheck ?? false,
        };
    }

    _lettersToValues(code) {
        return code.split('').map(c => c.match(/\d/) ? c : letterToNumber(c.toUpperCase())).join('');
    }
};
