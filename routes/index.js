const express = require('express');
const router = express.Router();
const createError = require('http-errors');
const generators = require('../src/generators');

const countries = Object.keys(generators).join('|');

router.get(`/:country(|${countries})`, function(req, res, next) {
    res.render('index', {
        generators: generators,
        baseUrl: req.protocol + '://' + req.get('host'),
        country: req.params.country.toUpperCase(),
        links: {
            website: {href: 'https://avris.it', icon: 'far fa-user', text: 'Andrea Vos'},
            email: {href: 'mailto:andrea@avris.it', icon: 'far fa-envelope', text: 'andrea@avris.it'},
            oql: {href: 'https://oql.avris.it/license?c=Andrea%20Vos%7Chttps://avris.it', icon: 'far fa-balance-scale-left', text: 'OQL'},
            gitlab: {href: 'https://gitlab.com/Avris/Generator', icon: 'fab fa-gitlab', text: 'Avris/Generator'},
            npm: {href: 'https://www.npmjs.com/package/avris-generator', icon: 'fab fa-npm', text: 'NPM'},
            bunq: {href: 'https://bunq.me/AndreaVos', icon: 'far fa-money-check-alt', text: 'Bank transfer'},
            paypal: {href: 'https://paypal.me/AndreAvris', icon: 'fab fa-paypal', text: 'PayPal'},
        },
        apiEndpoints: {
            'index.get': {method: 'GET', url: '/api'},
            'generate.get': {method: 'GET', url: '/api/:country/:generator?param=value'},
            'validate.get': {method: 'GET', url: '/api/:country/:generator/:number'},
            'barcode.svg': {method: 'GET', url: '/api/barcode/ean13/:number.svg'},
        },
    });
});

module.exports = router;
